//
//  IndieBookstore.swift
//  HomeworkApi
//
//  Created by Jessica on 2018/2/26.
//  Copyright © 2018年 Jessica. All rights reserved.
//

import Foundation

struct IndieBookstore: Codable {
    //option ?
    let name: String?
    let representImage :String?
    let intro :String?
    let address :String?
    let areaCode :Int?
    
    enum CodingKeys : String, CodingKey {
        case name = "name"
        case representImage = "representImage"
        case intro = "intro"
        case address = "address"
        case areaCode = "areaCode"
    }
}
