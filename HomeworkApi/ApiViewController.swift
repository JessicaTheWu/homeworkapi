//
//  ApiViewController.swift
//  HomeworkApi
//
//  Created by Jessica on 2018/2/26.
//  Copyright © 2018年 Jessica. All rights reserved.
//

import UIKit

class ApiViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var introTextView: UITextView!
    @IBOutlet weak var addressLabel: UILabel!
    
    var bookstore:IndieBookstore?
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = bookstore?.name
        introTextView.text = bookstore?.intro
        addressLabel.text = bookstore?.address
        
        let urlString = bookstore?.representImage
        let url = URL(string: urlString!)
        
        imageView.downloadedFrom(url: url!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
