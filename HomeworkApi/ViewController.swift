//
//  ViewController.swift
//  HomeworkApi
//
//  Created by Jessica on 2018/2/22.
//  Copyright © 2018年 Jessica. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var bookstores = [IndieBookstore]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let decoder = JSONDecoder()
        
        let url = "https://cloud.culture.tw/frontsite/trans/emapOpenDataAction.do?method=exportEmapJson&typeId=M"
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).validate().responseJSON { response in
                //避免取得的資料為nil
            
                if let json = response.data {
                    print("json: \(json)")
                    
                    do {
                        self.bookstores = try JSONDecoder.init().decode([IndieBookstore].self, from: json)
                        print("books are \(String(describing: self.bookstores))")
                    } catch let error {
                        print("\(error)")
                    }
                    
                }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
//            let result = response.result
//            if let dict = result.value as? Dictionary<String, IndieBookstore>  {
//                if let innerDict = dict["name"]{
//                    self.bookstores = [innerDict] as [IndieBookstore]
//                    self.tableView.reloadData()
//                }
//            }
            }
       
        
    }
    
    //the number of tableView rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookstores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = bookstores[indexPath.row].name ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showBookstore", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ApiViewController {
            destination.bookstore = bookstores[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

